const images = document.querySelectorAll('.image-to-show');
const stopButton = document.getElementById('stopButton');
const resumeButton = document.getElementById('resumeButton');
let currentImageIndex = 0;
let intervalId;

function showImage(index) {
    images.forEach((image, i) => {
        if (i === index) {
            image.style.display = 'block';
        } else {
            image.style.display = 'none';
        }
    });
}

function showNextImage() {
    currentImageIndex = (currentImageIndex + 1) % images.length;
    showImage(currentImageIndex);
}

function startSlideshow() {
    showImage(currentImageIndex);
    intervalId = setInterval(showNextImage, 3000);
}

function stopSlideshow() {
    clearInterval(intervalId);
}

function resumeSlideshow() {
    startSlideshow();
}

stopButton.addEventListener('click', () => {
    stopSlideshow();
});

resumeButton.addEventListener('click', () => {
    resumeSlideshow();
});

startSlideshow();